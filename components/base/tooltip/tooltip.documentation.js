import * as description from './tooltip.md';
import examples from './examples';

export default {
  description,
  examples,
  bootstrapComponent: 'b-tooltip',
};
