import PaginationBasicExample from './pagination.basic.example.vue';

export default [
  {
    name: 'Basic',
    items: [
      {
        id: 'pagination-basic',
        name: 'Basic',
        description: 'Basic Pagination',
        component: PaginationBasicExample,
      },
    ],
  },
];
