# gitlab-ui

`gitlab-ui` is a UI component library written in [Vue.js](https://vuejs.org).  
See https://gitlab-org.gitlab.io/gitlab-ui/ for documentation.

## Quick start

1. Clone the project with `git clone git@gitlab.com:gitlab-org/gitlab-ui.git`
1. Install [yarn](https://yarnpkg.com/en/)
1. Run `yarn storybook` to build and launch storybook to see the components in the browser
1. Open `http://localhost:9001/`

### Requirements
* Node

`$ npm install @gitlab-org/gitlab-ui`

## Contributing guide

Please refer to [CONTRIBUTING.md](CONTRIBUTING.md) for details on how to add new components and contribute in general.
